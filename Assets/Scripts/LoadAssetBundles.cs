﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoadAssetBundles : MonoBehaviour
{
    int loadedAssets = 0;
    string scenario;

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
            scenario = "x.1/";
        else if (SceneManager.GetActiveScene().buildIndex == 2)
            scenario = "x.2/";
        else if (SceneManager.GetActiveScene().buildIndex == 3)
            scenario = "x.3/";
        else if (SceneManager.GetActiveScene().buildIndex == 4)
            scenario = "x.4/";
        else if (SceneManager.GetActiveScene().buildIndex == 5)
            scenario = "x.5/";

        StartCoroutine(DownloadAsset("tower"));
        StartCoroutine(DownloadAsset("bridges"));
        StartCoroutine(DownloadAsset("logos"));
        StartCoroutine(DownloadAsset("building"));
        StartCoroutine(DownloadAsset("trees"));
        StartCoroutine(DownloadAsset("cars"));
        StartCoroutine(DownloadAsset("billboards"));
        StartCoroutine(DownloadAsset("rocks"));
        StartCoroutine(DownloadAsset("terrain"));
        StartCoroutine(DownloadAsset("tracks"));
    }

    IEnumerator DownloadAsset(string str)
    {
        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle("https://cp-calgary-yard.s3.ca-central-1.amazonaws.com/" + scenario + str))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);

                foreach (string asset in bundle.GetAllAssetNames())
                {
                    Debug.Log(asset);
                }

                var prefab = bundle.LoadAsset(str);
                Instantiate(prefab);

                loadedAssets++;
            }
        }

        if (loadedAssets == 10)
            EnablePlayer();
    }

    void EnablePlayer()
    {
        Destroy(GameObject.Find("LoadCanvas"));
        Destroy(GameObject.Find("LoadCamera"));
        GameObject v = GameObject.Find("VRTK");
        v.transform.GetChild(0).gameObject.SetActive(true);
        v.transform.GetChild(1).gameObject.SetActive(true);
    }
}
