﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameErrorDebugger : MonoBehaviour
{
    Text t;

    void Start()
    {
        t = GetComponent<Text>();
    }

    void OnEnable()
    {
        Application.logMessageReceived += LogCallback;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= LogCallback;
    }

    void LogCallback(string logString, string stackTrace, LogType type)
    {
        if (name.Equals("dt1"))
        {
            if (type == LogType.Warning)
                t.text += logString + "\n";
        }
        else if (name.Equals("dt2"))
        {
            if (type == LogType.Error)
                t.text += logString + "\n";
        }
        else if (name.Equals("dt3"))
        {
            if (type == LogType.Exception)
                t.text += logString + "\n";
        }
        else if (name.Equals("dt4"))
        {
            if (type == LogType.Log)
                t.text += logString + "\n";
        }
    }
}
